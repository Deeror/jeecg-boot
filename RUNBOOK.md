# 运行手册

## 本地运行
### 数据库
1. 初始化，Navicat直接执行：
    D:\Workspaces\IDEA\Opensource\jeecg-boot\jeecg-boot\db\jeecgboot-mysql-5.7.sql
2. 配置数据库连接
    jeecg-boot-module-system\src\main\resources\application-dev.yml

### 后端
> 先启动后台
1. 配置jvm启动参数：
> org.jeecg.JeecgSystemApplication
    -Dspring.profiles.active=dev
2. Run [jeecg-boot-module-system] org.jeecg.JeecgSystemApplication#main()
3. 验证
    http://localhost:8080/jeecg-boot/

### 前端
1. 开发模式运行
> 右击项目[ant-design-vue-jeecg] Open in Terminal
    npm run serve
2. 登录验证
http://localhost:3000/
    admin\123456



## 打包
### 指定(打包)环境
> 默认 dev
    jeecg-boot/pom.xml 中的 <activeByDefault> 节点

### Maven打包
    测服: mvn package -P test
    生产: mvn package -P prod

## 移动端效果
手机登录，可以正常显示图形报表
